import java.util.Random;
import net.ricecode.similarity.JaroWinklerStrategy;
import net.ricecode.similarity.SimilarityStrategy;
import net.ricecode.similarity.StringSimilarityService;
import net.ricecode.similarity.StringSimilarityServiceImpl;

public class Rename {
	
	public static void main(String[] arg){
		
		String name = "Lebron James";
		
		// Stragtage 1 replace letter.
		
		char[]  chars = name.toCharArray();
		int preloc = -1;
		
		//
		//People aren't born good or bad. Maybe they're born with tendencies either way, but its the way you live your life that matters.�? 
		//― Cassandra Clare, City of Glass
		//
		
		String message = "People aren't born good or bad. Maybe they're born with tendencies either way, but its the way you live your life that matters";
		
		char[] lib = message.toCharArray();
		for(int i =(int)chars.length/3;i>0;i--){
			
			int replace=-1;
			do{
				replace = (int)(Math.random() * chars.length-1); 
			}while(preloc==replace);
			
			preloc=replace;
			System.out.print(chars[replace]);
			chars[replace]= lib[(int)(Math.random() * lib.length-1)];
			System.out.println("    :"+chars[replace]);
			
		}
		StringBuilder result = new StringBuilder(chars.length);
		for (Character c : chars) {
		  result.append(c);
		}
		String output = result.toString();
		System.out.println(output);
		
		
		// Stragtage 2 replace name with similiar name .
		
		String name2 = "Lebron James";
		
		String name3 = "Kevin Durant Kobe Bryant";
		
		String[] name2List = name2.split(" ");
		
		String[] name3List = name3.split(" ");
		
		
		SimilarityStrategy strategy = new JaroWinklerStrategy();
		StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
		
		for(int j  = name2List.length-1; j>=0;j--){
			int replace = 0;
			double score = 0 ;
			for(int i = name3List.length-1; i>=0;i--){
				double score2 = service.score(name2List[j], name3List[i]);
				if(score2>score&&score!=1){
					replace = i;
					score=score2;
				}
			}
			
			name2List[j]= name3List[replace];
		}
		
		
		StringBuilder result2 = new StringBuilder(name2List.length);
		for (String c : name2List) {
		  result2.append(c);
		}
		String output2 = result2.toString();
		System.out.println(output2);
		
	}

}
